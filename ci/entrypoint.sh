#!/bin/sh
set -eu
envsubst '${BACKEND_URI}' < /etc/nginx/conf.d/guestbook.conf.template > /etc/nginx/conf.d/guestbook.conf
find '/usr/share/nginx/html' -name '*.js' -exec sed -i -e 's,BACKEND_URI,'"$BACKEND_URI"',g' {} \;
find '/usr/share/nginx/html' -name '*.js' -exec sed -i -e 's,GUESTBOOK_NAME,'"$GUESTBOOK_NAME"',g' {} \;
exec "$@"

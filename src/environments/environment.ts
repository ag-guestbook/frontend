export const environment = {
  production: true,
  guestBookName: 'GUESTBOOK_NAME',
  backendUri: 'https://jokeclub.me/guestbook/'
};

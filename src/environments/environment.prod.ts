export const environment = {
  production: true,
  guestBookName: 'GUESTBOOK_NAME',
  backendUri: 'https://www.jokeclub.me:3000/guestbook/'
};
